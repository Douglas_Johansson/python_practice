import cv2 as cv
import numpy as np
from time import perf_counter
face_cascade = cv.CascadeClassifier(cv.data.haarcascades +'haarcascade_frontalface_default.xml') ## Used in detecting FACE
eye_cascade = cv.CascadeClassifier(cv.data.haarcascades +'haarcascade_eye.xml')
smile_cascade = cv.CascadeClassifier(cv.data.haarcascades +'haarcascade_smile.xml')

# Params relative name to path, name of window
def readImage(source, windowname):
    img = cv.imread(source)
    cv.imshow(windowname, img)
    cv.waitKey(0)

def toGray(frame):
    return cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

def addText(frame, text, width, height):
    return cv.putText(frame, text, (width, height), cv.FONT_HERSHEY_SIMPLEX, 1, (220, 220, 220), 2, cv.LINE_AA)

def blendImage(frame, width , height, fade):
    tux = cv.imread('opencv\images\\Tux.png')
    tux = cv.resize(tux, (width, height))
    dst = cv.addWeighted(frame, 0.5, tux , fade , 0)
    return dst

def pixelate(frame):
   width, height = (16, 16)
   temp = cv.resize(frame, (width, height), interpolation=cv.INTER_LINEAR)
   output = cv.resize(temp, (640, 480), interpolation=cv.INTER_NEAREST)
   return output

def detect(gray, frame):
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x, y, w, h) in faces:
        center_coordinates = x + w // 2, y + h // 2
        radius = w // 2 
        cv.circle(frame, center_coordinates, radius, (51, 255, 255), -1)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            radius = ew // 2 
            center_coordinates = ex + ew // 2, ey + eh // 2 
            cv.circle(roi_color, center_coordinates, radius, (0, 0, 0), -1) 
    return frame

def colorChange(frame):
    orangeLow = np.array([5, 50, 50])
    orangeHigh = np.array([15, 255, 255])
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    mask = cv.inRange(hsv, orangeLow, orangeHigh)

    frame[mask > 0] = (0, 255, 0)
    return frame

def main():
    capture = cv.VideoCapture(0)  
    fourcc = cv.VideoWriter_fourcc(*'XVID')
    out = cv.VideoWriter('output.avi', fourcc, 30.0, (640, 480))
    time = perf_counter()    
    textWidth = 0 
    textHeight = 300
    fade = 0

    while capture.isOpened():
        isTrue, frame = capture.read()
        nextFrame = frame
        passedTime = perf_counter() - time

        if passedTime < 10:
            nextFrame = addText(frame, "Greetings", textWidth, textHeight)
            textWidth += 3
        elif passedTime < 20 :
            nextFrame = pixelate(frame)
        elif passedTime < 30 :
            nextFrame = toGray(frame)
        elif passedTime < 40 :
            nextFrame = detect(cv.cvtColor(frame, cv.COLOR_BGR2GRAY), frame)
        elif passedTime < 50 :
            nextFrame = blendImage(frame, 640, 480, fade)
            fade += 0.01
        elif passedTime < 60 :
            nextFrame = colorChange(frame)
        elif passedTime < 70:
            break               
        out.write(nextFrame)
        cv.imshow('OpenCV Practice', cv.resize(nextFrame, (640, 480)))
        if cv.waitKey(20) & 0xFF==ord('q'): # qqif q is pressed do something 
            break

    capture.release()
    cv.destroyAllWindows()

main()

