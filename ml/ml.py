#!/usr/bin/env python

import cv2 as cv
import numpy as np

SZ=72
bin_n = 16 # Number of bins


affine_flags = cv.WARP_INVERSE_MAP|cv.INTER_LINEAR

def deskew(img):
    m = cv.moments(img)
    if abs(m['mu02']) < 1e-2:
        return img.copy()
    skew = m['mu11']/m['mu02']
    M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
    img = cv.warpAffine(img,M,(SZ, SZ),flags=affine_flags)
    return img

## [hog]
def hog(img):
    gx = cv.Sobel(img, cv.CV_32F, 1, 0)
    gy = cv.Sobel(img, cv.CV_32F, 0, 1)
    mag, ang = cv.cartToPolar(gx, gy)
    bins = np.int32(bin_n*ang/(2*np.pi))    # quantizing binvalues in (0...16)
    bin_cells = bins[:SZ//2,:SZ//2], bins[SZ//2:,:SZ//2], bins[:SZ//2,SZ//2:], bins[SZ//2:,SZ//2:]
    mag_cells = mag[:SZ//2,:SZ//2], mag[SZ//2:,:SZ//2], mag[:SZ//2,SZ//2:], mag[SZ//2:,SZ//2:]
    hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]
    hist = np.hstack(hists)     # hist is a 64 bit vector
    return hist

img = cv.imread('better_test.png',0)
if img is None:
    raise Exception("we need the digits.png image from samples/data here !")
width = 720
height = 576
dim = (width, height)
print (img)
img = cv.resize(img, dim, cv.INTER_AREA)
print (img.shape)
cells = [np.hsplit(row, 10) for row in np.vsplit(img,8)]

for x in range(8):
    img = cv.line(img,(0,72*x),(720,72*x),(255,0,0),1)
    
for y in range(10):
    img = cv.line(img,(72*y,0),(72*y,576),(255,0,0),1)

while True:
    cv.imshow('img', cv.resize(img, (720,576)))
    if cv.waitKey(1) == ord('q'):
        break
cv.destroyAllWindows()
# First half is trainData, remaining is testData
train_cells = [ i[:5] for i in cells ]
test_cells = [ i[5:] for i in cells]

######     Now training      ########################

deskewed = [list(map(deskew,row)) for row in train_cells]
hogdata = [list(map(hog,row)) for row in deskewed]
trainData = np.float32(hogdata).reshape(-1,64)
responses = np.repeat(np.arange(4), 10)[:,np.newaxis]

svm = cv.ml.SVM_create()
svm.setKernel(cv.ml.SVM_LINEAR)
svm.setType(cv.ml.SVM_C_SVC)
svm.setC(2.67)
svm.setGamma(5.383)

svm.train(trainData, cv.ml.ROW_SAMPLE, responses)
svm.save('svm_data.dat')

######     Now testing      ########################

deskewed = [list(map(deskew,row)) for row in test_cells]
hogdata = [list(map(hog,row)) for row in deskewed]
testData = np.float32(hogdata).reshape(-1,bin_n*4)
result = svm.predict(testData)[1]

#######   Check Accuracy   ########################
mask = result==responses
correct = np.count_nonzero(mask)
print(correct*100.0/result.size)