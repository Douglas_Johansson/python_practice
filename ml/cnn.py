from cv2 import WINDOW_AUTOSIZE
import tensorflow as tf
import cv2 as cv
from tensorflow.keras import datasets, layers, models
import matplotlib.pyplot as plt
import numpy as np



img = cv.imread('machinelearning\\better_test.png')
if img is None:
    raise Exception("we need the digits.png image from samples/data here !")

width = 720
height = 576
dim = (width, height)
img = cv.resize(img, dim, cv.INTER_AREA)
size = 72




clone = img.copy()

cells = [np.hsplit(row, 10) for row in np.vsplit(img,8)]
train_cells = [ i[:5] for i in cells ]
test_cells = [ i[5:] for i in cells]
train_array = np.array(train_cells)
test_array = np.array(test_cells)

flatTrain = train_array.reshape(40, 72, 72, 3)
flatTest = test_array.reshape(40, 72, 72, 3)
class_names = ['A', 'B', 'C', 'D']
trainLabels = np.repeat(np.arange(len(class_names)), 10)[:,np.newaxis]
testLabels = np.repeat(np.arange(len(class_names)), 10)[:,np.newaxis]

""" plt.figure(figsize=(size,size))
for i in range(40):
    plt.subplot(4,10,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(flatTrain[i])
    # The CIFAR labels happen to be arrays, 
    # which is why you need the extra index
plt.show() """
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(72, 72, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(10))

model.compile(optimizer='adam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

history = model.fit(flatTrain, trainLabels, epochs=10, 
                    validation_data=(flatTest, testLabels))

plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')

test_loss, test_acc = model.evaluate(flatTrain,  trainLabels, verbose=2)
print(test_acc)
plt.show()
