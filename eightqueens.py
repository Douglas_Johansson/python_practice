# From https://www.youtube.com/watch?v=MozDUrcChCA

import random
import time
numQueens = 8
currentSolution = [0 for x in range(numQueens)]  # will hold current testing data
solutions = []  # holds found solutions

def isSafe(testRow, testCol):
    # no need to check for row 0
    if testRow == 0:
        return True

    for row in range(0, testRow):

        # check vertical
        if testCol == currentSolution[row]:
            return False

        # diagonal
        if abs(testRow - row) == abs(testCol - currentSolution[row]):
            return False

    # no attack found
    return True

def placeQueen(row):    # Recursive function to place Queens on board
    global currentSolution, solutions, numQueens

    for col in range(numQueens):
        if not isSafe(row, col):
            continue
        else:
            currentSolution[row] = col
            if row == (numQueens - 1): # When all queens have been succesfully placed on the board, copy to solutions array
                #  last row
                solutions.append(currentSolution.copy())
            else:
                placeQueen(row + 1)
                                                    
def printBoard(solutions): ## Print one of the solutions in the console
    thisSolution = random.choice(solutions)
    for element in thisSolution:
        for x in range(8):
            if x == element :
                    print( 'Q', end=' ')         
            else:
                print( '0',end =' ')
        print()


placeQueen(0) # Call the function starting from first row

printBoard(solutions) # Print board



       

        
    


